//
//  FileOperation.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/17.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation

class FileOperation {
    
    var bufferSize: Int = 1024
    fileprivate var buffer: NSMutableData
    fileprivate var filePath: String
    let fileManager = FileManager.default
    
    init() {
        buffer = NSMutableData()
        let docPath = NSHomeDirectory() + "/Documents/ECG"
        if !fileManager.fileExists(atPath: docPath, isDirectory: nil) {
            try! fileManager.createDirectory(atPath: docPath, withIntermediateDirectories: true, attributes: nil)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let fileName = dateFormatter.string(from: Date()) + ".ecg"
        filePath = NSHomeDirectory() + ("/Documents/ECG/" + fileName)
    }
    
    func append(_ data: Data) {
        if !fileManager.fileExists(atPath: filePath) {
            fileManager.createFile(atPath: filePath, contents: nil, attributes: nil)
        }
        
        buffer.append(data)
        if buffer.length >= bufferSize {
            //写入文件
            let fileHandle = FileHandle(forWritingAtPath: filePath)
            fileHandle?.seekToEndOfFile()
            fileHandle?.write(buffer as Data)
            //恢复buffer
            buffer = NSMutableData()
        }
    }
    
    class func removeFile(_ fileName: String) {
        let fileManager = FileManager.default
        let filePath = NSHomeDirectory() + ("/Documents/ECG" + fileName)
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch {
            print("删除文件失败")
        }
        
    }
}
