//
//  ViewController.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/8.
//  Copyright © 2016年 Zero. All rights reserved.
//

import UIKit
import CoreBluetooth

@available(iOS 10.0, *)
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var bleList = [CmdDiscovery]()
    let centerManager = CmdCentralManager.manager
    let fileOp = FileOperation()
    
    override func loadView() {
        super.loadView()
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        print(centerManager.connectedStatus)
//        print(centerManager.currentConnectedPeripheral)
        
//        centerManager.autoConnect = true
//        centerManager.reconnectWithIdentifier("337A2C9D-99D3-2DC2-4037-DBAA63E0B22D", duration: 5, success: { (central, peripheral) in
//            print("success")
//            }, fail: { (error) in
//                print("fail")
//        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(parseECGData(_:)), name: NSNotification.Name(rawValue: CmdNotifyECGData), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(retriveFinish), name: NSNotification.Name(rawValue: CmdRetriveFinishNotify), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(readRSSI(_:)), name: NSNotification.Name(rawValue: CmdReadRSSINotify), object: nil)
//        
//        Timer.scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(refresh), userInfo: nil, repeats: true)
    }
    
    func retriveFinish() {
        print("蓝牙服务检索完毕")
//        print(centerManager.connectedStatus)
//        print(centerManager.currentConnectedPeripheral)
    }
    
    func readRSSI(_ notify: Notification) {
        let data = notify.object as! NSNumber
        print(data)
    }
    
    func refresh() {
        ReadRSSI.readRSSI()
//        leadView.drawing()
    }
    
//    func parseECGData(_ notify: Notification) {
//        let data = notify.object as! Data
//        guard let (status, index, ecgs, originEcgData)  = ECGDataParser.parse(data) else {
//            return
//        }
//        
//        
////        fileOp.append(data)
//    }
    
    
    
    @IBAction func readBattery(_ sender: AnyObject) {
        ReadBattery().readBattery({
                print("当前电量: " + "\($0)")
            ReadBattery().readBattery({ (bat) in
                print("嵌套电量")
                }, failure: nil)
            }) {
                print($0)
        }
    }

    @IBAction func callBackTest(_ sender: AnyObject) {
//        ControlCommand().startControllCmd(.StartECG(time: 5000), success: {
//            print("开始ECG")
//            }, failure: nil)
        
        
    }
    
    @IBAction func scan(_ sender: AnyObject) {
        self.bleList.removeAll()
        centerManager.scanWithServices(nil, duration: 3, discoveryHandle: {
            self.bleList.append($0)
            self.tableView.reloadData()
            }, completeHandle: nil)
    }
    
    //MARK: - TableView,DataSource Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "BLECELL"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellId)
        }
        cell!.textLabel!.text = bleList[(indexPath as NSIndexPath).row].peripheral.name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        centerManager.connect(bleList[(indexPath as NSIndexPath).row], duration: 10, success: { (central, discovery) in
            print("success")
        }) { (error) in
            print("connect fail")
        }
    }
    
    deinit {
        print("[Release: ] __ViewController Release!")
    }
}

