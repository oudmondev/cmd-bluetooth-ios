//
//  QueryMacAddress.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation
import CoreBluetooth

class QueryMacAddress: BaseCommand {
    typealias MacAddressHandle = (_ nrfMac: String, _ appMac: String) -> Void
    fileprivate var macAddressHandle: MacAddressHandle?
    
    func queryMacAddress(_ macAddress: MacAddressHandle?, failure: FailureHandle?) {
        self.failureHandle = failure
        macAddressHandle = macAddress
        if !super.start() {
            return
        }
        self.timeoutInterval = 0.1
        super.write(QueryCmd.macAddress.cmdData, repeatCount: 3)
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == PlanetoidParser.notifyCharacterUUIDStr else { return }
        if currentStage == 0 {
            _ = super.VerifyCmdResponse(Int(QueryCmd.macAddress.cmdHeader), backData: data)
            self.currentStage += 1
            return
        }
        
        let bytes = CmdHelper.dataToBytes(data)
        let nrfAdd = CmdHelper.stringFromNumArr(bytes, rang: (location: 2, length: 6), separator: ":")
        let appAdd = CmdHelper.stringFromNumArr(bytes, rang: (location: 8, length: 6), separator: ":")
        super.finish()
        macAddressHandle?(nrfAdd, appAdd)
    }
}
