//
//  ControlCommand.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/12.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation
import CoreBluetooth

class ControlCommand: BaseCommand {
    fileprivate var successHandle: (() -> Void)?
    fileprivate var curType: ControlCmd = .startHR
    func startControllCmd(_ type: ControlCmd, success: (() -> Void)?, failure: FailureHandle?) {
        self.failureHandle = failure
        successHandle = success
        curType = type
        if !super.start() {
            return
        }
        self.timeoutInterval = 0.1
        super.write(type.cmdData, repeatCount: 3)
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == PlanetoidParser.notifyCharacterUUIDStr else { return }
        if currentStage == 0 {
            if data.count != 4 { return }
            _ = super.VerifyCmdResponse(Int(curType.cmdHeader), backData: data)
            self.currentStage += 1
            return
        }
        
        var confirmByte: Int = 0
        (data as NSData).getBytes(&confirmByte, range: NSRange(location: 2, length: 1))
        super.finish()
        confirmByte == 0 ? successHandle?() : self.failureHandle?(.wrongResponse)
    }
}
