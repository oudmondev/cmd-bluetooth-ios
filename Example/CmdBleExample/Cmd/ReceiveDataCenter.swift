//
//  ReceiveDataCenter.swift
//  Planetoid
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation
import CoreBluetooth

let CmdNotifyECGData = "CmdNotify_ECGData"
let CmdNotifyHRData = "CmdNotify_HRData"

//class ReceiveDataCenter: NSObject, ParserDataReceiveDelegate {
//    
//    func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
//        if characteristic.uuid.uuidString == "2A37" {
//            HRDataParser.parse(data)
//            NotificationCenter.default.post(name: Notification.Name(rawValue: CmdNotifyHRData), object: data)
//        }
//        if characteristic.uuid.uuidString == "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: CmdNotifyECGData), object: data)
//            ECGDataParser.parse(data)
//        }
//    }
//}
