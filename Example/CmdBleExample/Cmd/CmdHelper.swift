//
//  CmdHelper.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation

class CmdHelper {
    static func bytesToData(_ bytes: [UInt8]) -> Data {
        var byteArr:[UInt8] = [UInt8]()
        byteArr.append(contentsOf: bytes)
        return Data(bytes: UnsafePointer<UInt8>(byteArr), count: byteArr.count)
    }
    
    static func dataToBytes(_ data: Data) -> [UInt8] {
        var byteArr:[UInt8] = [UInt8]()
        for i in 0...data.count - 1 {
            var byte: UInt8 = 0
            (data as NSData).getBytes(&byte, range: NSRange(location: i, length: 1))
            byteArr.append(byte)
        }
        return byteArr
    }
    
    static func cmdBackVerify(_ cmd: Int, backData: Data) -> Bool {
        var cmdByte: Int = 0
        var legalByte: Int = 0
        (backData as NSData).getBytes(&cmdByte, range: NSRange(location: 2, length: 1))
        (backData as NSData).getBytes(&legalByte, range: NSRange(location: 3, length: 1))
        return cmdByte == cmd && legalByte == 0
    }
    /**
     bytes[Uint8]转化为字符串，中间分隔号，比如Mac地址
     */
    static func stringFromNumArr(_ bytes:[UInt8], rang: (location: Int, length: Int), separator: String) -> String {
        var strBytes = [String]()
        for i in 0..<rang.length {
            strBytes.append(String(format: "%02X", bytes[rang.location + i]))
        }
        return strBytes.joined(separator: separator)
    }
    /**
     UInt32 -> NSData (使用小端)
     */
    static func UInt32ToDataByLittleEndian(_ num: UInt32) -> Data {
        var bytes = [UInt8]()
        for i in 0..<4 {
            let byte = (UInt32(0xff << (i * 8)) & num) >> UInt32(i * 8)
            bytes.append(UInt8(byte))
        }
        return Data(bytes: UnsafePointer<UInt8>(bytes), count: 4)
    }
    /**
     UInt16 -> NSData (使用小端)
     */
    static func UInt16ToDataByLittleEndian(_ num: UInt16) -> Data {
        var bytes = [UInt8]()
        for i in 0..<2 {
            let byte = (UInt16(0xff << (i * 8)) & num) >> UInt16(i * 8)
            bytes.append(UInt8(byte))
        }
        return Data(bytes: UnsafePointer<UInt8>(bytes), count: 2)
    }
}
