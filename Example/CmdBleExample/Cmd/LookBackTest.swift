//
//  LookBackTest.swift
//  CmdBluetooth
//
//  Created by Zero on 16/8/5.
//  Copyright © 2016年 Zero. All rights reserved.
//

import UIKit
import CoreBluetooth

class LookBackTest: BaseCommand {

    func lookBackTest() {
        if !super.start() {
            return
        }
        super.write(self.testData(), repeatCount: 3)
    }
    
    func testData() -> Data {
        var byteArr:[UInt8] = [UInt8]()
        byteArr.append(0x04)
        byteArr.append(0x91)
        byteArr.append(0x12)
        byteArr.append(0x56)
        
        let data = Data(bytes: UnsafePointer<UInt8>(byteArr), count: 4)
        return data
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" else { return }
        print("\(data) + \(characteristic)")
//        super.finish()
        self.timeoutInterval = 10.5
        super.updateTimer()
    }
    
    deinit {
        print("[Release: ] __LookBackTest release")
    }
}
