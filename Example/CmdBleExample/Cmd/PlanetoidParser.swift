//
//  MySelfPeripheralParser.swift
//  BleCore
//
//  Created by Vincent on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation

class PlanetoidParser: CmdBaseParser {

    static let notifyCharacterUUIDStr = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
    static let writeCharacterUUIDStr = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
    static let readNotifyBatteryCharacterUUIDStr = "2A19"
    
    /**
        define types for writing data to BLE device, like this
     */
    func writeDataWithResponse(_ data: Data) {
        do {
            try super.writeData(data, characterUUIDStr: PlanetoidParser.writeCharacterUUIDStr, withResponse: true)
        } catch let error {
            print("[Error: ]__Write Data Error    " + "\(error)")
        }
    }
    
    func writeDataWithoutResponse(_ data: Data) {
        do {
            try super.writeData(data, characterUUIDStr: PlanetoidParser.writeCharacterUUIDStr, withResponse: false)
        } catch let error {
            print("[Error: ]__Write Data Error    " + "\(error)")
        }
    }
    
    func readData(_ characterUUIDStr: String) {
        do {
            try super.readCharacteristic(characterUUIDStr)
        } catch let error {
            print("[Error: ]__Read Data Error    " + "\(error)")
        }
    }
    
    //......Many....many ^_^!
}
