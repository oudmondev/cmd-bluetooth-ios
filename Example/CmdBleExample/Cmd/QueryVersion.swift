//
//  QueryVersion.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation
import CoreBluetooth

class QueryVersion: BaseCommand {
    
    typealias VersionHandle = (_ hard: String, _ total: String, _ stm32: String, _ nrf: String) -> Void
    fileprivate var versionHandle: VersionHandle?
    func queryVersion(_ version: VersionHandle?, failure: FailureHandle?) {
        self.failureHandle = failure
        versionHandle = version
        if !super.start() {
            return
        }
        self.timeoutInterval = 0.1
        super.write(QueryCmd.version.cmdData, repeatCount: 3)
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == PlanetoidParser.notifyCharacterUUIDStr else { return }
        if currentStage == 0 {
            _ = VerifyCmdResponse(Int(QueryCmd.version.cmdHeader), backData: data)
            self.currentStage += 1
            return
        }
        
        let bytes = CmdHelper.dataToBytes(data)
        let hardV = CmdHelper.stringFromNumArr(bytes, rang: (location: 2, length: 3), separator: ".")
        let totalV = CmdHelper.stringFromNumArr(bytes, rang: (location: 5, length: 3), separator: ".")
        let stm32V = CmdHelper.stringFromNumArr(bytes, rang: (location: 8, length: 3), separator: ".")
        let nrfV = CmdHelper.stringFromNumArr(bytes, rang: (location: 11, length: 3), separator: ".")
        
        super.finish()
        versionHandle?(hardV, totalV, stm32V, nrfV)
    }
}
