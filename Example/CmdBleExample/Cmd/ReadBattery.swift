//
//  ReadBattery.swift
//  CmdBluetooth
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//

import UIKit
import CoreBluetooth

class ReadBattery: BaseCommand {
    
    fileprivate var batteryHandle: ((Int) -> Void)?
    func readBattery(_ battery: ((Int) -> Void)?, failure: FailureHandle?) {
        self.failureHandle = failure
        batteryHandle = battery
        if !super.start() {
            return
        }
        super.read(PlanetoidParser.readNotifyBatteryCharacterUUIDStr)
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == PlanetoidParser.readNotifyBatteryCharacterUUIDStr else { return }
        var battery: Int = 0
        (data as NSData).getBytes(&battery, length: 1)
        super.finish()
        batteryHandle?(battery)
    }
}
