//
//  BaseCommand.swift
//  BleCore
//
//  Created by Zero on 16/8/10.
//  Copyright © 2016年 Zero. All rights reserved.
//


import Foundation
import CoreBluetooth

enum CmdFailureError: Error {
    case timeout
    case channelUnAvailable
    case bleDisconnect
    case wrongResponse
}

class BaseCommand:NSObject, ParserDelegate {
    
    typealias FailureHandle = ((_ error: CmdFailureError) -> Void)
    var parserSession: CmdParserSession?
    var timeoutInterval = 0.5
    var failureHandle: FailureHandle?
    var currentStage = 0
    fileprivate var timer: Timer?
    fileprivate var repeatCount = 3
    fileprivate var data: Data?
    
    override init() {
        super.init()
        self.parserSession = CmdCentralManager.manager.parser
        NotificationCenter.default.addObserver(self, selector: #selector(stateChange(_:)), name: NSNotification.Name(rawValue: CmdCentralStateNotify), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stateChange(_:)), name: NSNotification.Name(rawValue: CmdConnectStateNotify), object: nil)
    }
    
    func start() -> Bool {
        guard let parserSession = self.parserSession , parserSession.isFree && parserSession.connected else {
            failureHandle?(.channelUnAvailable)
            return false
        }
        parserSession.isFree = false
        parserSession.parserDelegate = self
        return true
    }
    
    func write(_ data: Data) {
        self.data = data
        (self.parserSession as? PlanetoidParser)?.writeDataWithoutResponse(data)
    }
    
    func write(_ data: Data, repeatCount: Int = 1) {
        self.repeatCount = repeatCount
        write(data)
        startTimer()
    }
    
    func read(_ characterUUIDStr: String) {
        (self.parserSession as? PlanetoidParser)?.readData(characterUUIDStr)
        startTimer()
    }
    
    func stateChange(_ notify: Notification) {
        if !(notify.object as! Bool) {
            self.finish()
            failureHandle?(.bleDisconnect)
            print("停止传输" + "\(notify.object)")
        }
    }
    
    func startTimer() {
        if self.timeoutInterval == 0 {
            self.timeoutInterval = 0.5
        }
        self.timer = Timer.scheduledTimer(timeInterval: self.timeoutInterval, target: self, selector: #selector(self.timerEnd), userInfo: nil, repeats: false)
        RunLoop.current.add(self.timer!, forMode: RunLoopMode.commonModes)
    }
    
    func updateTimer() {
        if let timer = self.timer , timer.isValid {
            timer.fireDate = Date().addingTimeInterval(self.timeoutInterval)
        }
    }
    
    func timerEnd() {
        repeatCount -= 1
        if repeatCount <= 0 {
            self.finish()
            failureHandle?(.timeout)
        } else {
            print("重发")
            if let data = self.data {
                write(data)
                startTimer()
            }
        }
    }
    
    func finish() {
        self.invalidTimer()
        parserSession?.isFree = true
        parserSession?.parserDelegate = nil
    }
    
    func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic:CBCharacteristic) {
        //override by subclass
    }
    
    func VerifyCmdResponse(_ cmd: Int, backData: Data) -> Bool {
        if !CmdHelper.cmdBackVerify(cmd, backData: backData) {
            self.finish()
            self.failureHandle?(.wrongResponse)
            return false
        }
        self.timeoutInterval = 0.5
        self.updateTimer()
        return true
    }
    
    func didWriteData(_ peripheral: CBPeripheral, characteristic:CBCharacteristic, error: NSError?) {
        //override by subclass
    }
    
    deinit {
        //self deinit
        self.parserSession = nil
        print("[Release: ]__BaseCommand release")
    }
    
    //MARK: - private method
    
    fileprivate func invalidTimer() {
        guard let timer = self.timer , timer.isValid else {
            return
        }
        self.timer!.invalidate()
        self.timer = nil
    }
}
