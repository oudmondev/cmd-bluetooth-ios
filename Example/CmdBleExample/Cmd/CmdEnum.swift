//
//  CmdEnum.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/12.
//  Copyright © 2016年 Zero. All rights reserved.
//

/**
 *  控制协议:   需要设备的二次回复,以便知道是否控制成功
 *  设置协议:   设置一次，不需要管是否写入成功.
 *  读取协议(透传):      获取返回值
 *  读取协议(标准):      一次返回，直接解析
 */

import Foundation

// MARK: Query Command -

enum QueryCmd {
    case version                //读取版本信息
    case macAddress             //读取Mac地址
}

extension QueryCmd {
    var cmdHeader: UInt8 {
        switch self {
        case .version:
            return 0x12
        case .macAddress:
            return 0x13
        }
    }
    
    var cmdData: Data {
        return CmdHelper.bytesToData([0x02, cmdHeader])
    }
}

//MARK: CONTROLL CMDS -

enum ControlCmd {
    case startHR                //开启心率
    case stopHR                 //停止心率
    case startECG(time: UInt32) //开启心电 time:测量时间
    case stopECG                //停止心电
    case startStep              //开始计步
    case stopStep               //停止计步
}

extension ControlCmd {
    var cmdHeader: UInt8 {
        switch self {
        case .startHR:
            return 0x21
        case .stopHR:
            return 0x22
        case .startECG:
            return 0x31
        case .stopECG:
            return 0x32
        case .startStep:
            return 0x41
        case .stopStep:
            return 0x42
        }
    }
    
    var cmdData: Data {
        switch self {
        case .startECG(let time):
            let data = NSMutableData()
            let headData = CmdHelper.bytesToData([0x06, 0x31])
            let content = CmdHelper.UInt32ToDataByLittleEndian(time)
            data.append(headData)
            data.append(content)
            return data as Data
        default:
            return CmdHelper.bytesToData([0x02, cmdHeader])
        }
    }
}

//MARK: SETTING CMDS -

enum BleUserGender: UInt8 {
    case unknow = 0
    case female = 1
    case male = 2
}

enum SettingCmd {
    case setTime(date: Date)          //设置时间 date:需要被设置的时间
    case setBleName(name: String)       //设置蓝牙名称 name: 需要被设置的名称
    case setUserInfo(gender: BleUserGender, age: UInt8, height: UInt16, weight: UInt16)   //设置用户信息
}

extension SettingCmd {
    var cmdHeader: UInt8 {
        switch self {
        case .setTime:
            return 0x14
        case .setBleName:
            return 0x15
        case .setUserInfo:
            return 0x51
        }
    }
    
    var cmdData: Data {
        switch self {
        case .setTime(let date):
            let data = NSMutableData()
            let headData = CmdHelper.bytesToData([0x06, cmdHeader])
            let content = CmdHelper.UInt32ToDataByLittleEndian(UInt32(date.timeIntervalSince1970))
            data.append(headData)
            data.append(content)
            return data as Data
        case .setBleName(let name):
            let data = NSMutableData()
            let headData = CmdHelper.bytesToData([UInt8(name.characters.count + 3), cmdHeader, UInt8(name.characters.count)])
            let content = name.data(using: String.Encoding.ascii)
            data.append(headData)
            data.append(content!)
            return data as Data
        case .setUserInfo(let gender, let age, let height, let weight):
            let data = NSMutableData()
//            let headData = CmdHelper.bytesToData([0x08, cmdHeader])
//            data.append(headData)
//            data.append(Data(bytes: UnsafePointer<UInt8>([gender]), count: 1))
//            data.append(Data(bytes: UnsafePointer<UInt8>([age]), count: 1))
//            let heightData = CmdHelper.UInt16ToDataByLittleEndian(height)
//            let weightData = CmdHelper.UInt16ToDataByLittleEndian(weight)
//            data.append(heightData)
//            data.append(weightData)
            return data as Data
        }
    }
}
