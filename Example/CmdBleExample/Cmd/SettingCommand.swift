//
//  SettingCommand.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/12.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation
import CoreBluetooth

class SettingCommand: BaseCommand {
    fileprivate var successHandle: (() -> Void)?
    fileprivate var curType: SettingCmd = .setTime(date: Date())
    func startSetting(_ type: SettingCmd, success: (() -> Void)?, failure: FailureHandle?) {
        self.failureHandle = failure
        successHandle = success
        curType = type
        if !super.start() {
            return
        }
        self.timeoutInterval = 0.1
        super.write(type.cmdData, repeatCount: 3)
    }
    
    override func receiveData(_ data: Data, peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        guard characteristic.uuid.uuidString == PlanetoidParser.notifyCharacterUUIDStr else { return }
        if data.count == 4 && super.VerifyCmdResponse(Int(curType.cmdHeader), backData: data) {
            currentStage += 1
            super.finish()
            successHandle?()
        }
    }
}
