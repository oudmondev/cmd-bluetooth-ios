//
//  ECGDataParser.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/15.
//  Copyright © 2016年 Zero. All rights reserved.
//

import Foundation

class ECGDataParser {
    fileprivate static let FORMAT_UINT32 = 4
    fileprivate static let FORMAT_UINT16 = 2
    
//    static func parse(_ value: Data?) -> (status: Int, index: Int, ecgs: [Int], originEcgData: Data)? {
//        guard let value = value , value.count > 6 else { return nil }
//        var cmdHeder = 0
//        (value as NSData).getBytes(&cmdHeder, range: NSMakeRange(1, 1))
//        if cmdHeder != 0x33 {
//            return nil
//        }
//        
//        var payload_Info = 0, offset = 2
//        (value as NSData).getBytes(&payload_Info, range: NSMakeRange(offset, FORMAT_UINT32))
//        offset += FORMAT_UINT32
//        let leadStatus = payload_Info >> 30
//        let index = payload_Info & 0x3FFFFFFF
//        print("状态:" + "\(leadStatus)" + "索引:" + "\(index)" )
//        
//        let oriData = value.subdata(in: NSMakeRange(offset, value.count - offset))
//        
//        var ecgArr = [Int]()
//        while offset < value.count {
//            var ecg: Int16 = 0
//            (value as NSData).getBytes(&ecg, range: NSMakeRange(offset, FORMAT_UINT16))
//            offset += 2
//            ecgArr.append(Int(ecg))
//        }
//        
//        return (status: leadStatus, index: index, ecgs: ecgArr, originEcgData: oriData)
//    }
}
