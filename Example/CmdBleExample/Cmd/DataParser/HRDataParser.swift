//
//  HRDataParser.swift
//  CmdBleExample
//
//  Created by Zero on 16/8/15.
//  Copyright © 2016年 Zero. All rights reserved.
//
//  参考资料: https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml

import Foundation

class HRDataParser {
    fileprivate static let HEART_RATE_VALUE_FORMAT = 0x01
    fileprivate static let SENSOR_CONTACT_STATUS = 0x06
    fileprivate static let ENERGY_EXPANDED_STATUS = 0x08
    fileprivate static let RR_INTERVAL = 0x10
    
    fileprivate static let FORMAT_UINT8 = 1
    fileprivate static let FORMAT_UINT16 = 2
    
    static func parse(_ value: Data?) {
        guard let value = value else { return }
        var offset = 0, flags = 0
        (value as NSData).getBytes(&flags, range: NSMakeRange(offset, FORMAT_UINT8))
        offset += 1
        
        let value16bit = (flags & HEART_RATE_VALUE_FORMAT) > 0
        let sensorContactStatus = (flags & SENSOR_CONTACT_STATUS) >> 1
        let energyExpandedStatus = (flags & ENERGY_EXPANDED_STATUS) > 0
        let rrIntervalStatus = (flags & RR_INTERVAL) > 0
        
        var heartRateValue = 0
        (value as NSData).getBytes(&heartRateValue, range: NSMakeRange(offset, value16bit ? FORMAT_UINT16 : FORMAT_UINT8))
        offset += (value16bit ? FORMAT_UINT16 : FORMAT_UINT8)
        print("心率值:" + "\(heartRateValue)")
        
        var energyExpanded = -1
        if energyExpandedStatus {
            (value as NSData).getBytes(&energyExpanded, range: NSMakeRange(offset, FORMAT_UINT16))
        }
        offset += FORMAT_UINT16
        print("能量:" + "\(energyExpanded)")
        
        var rrIntervals = [Float]()
        if rrIntervalStatus {
            while offset < value.count {
                var units = 0
                (value as NSData).getBytes(&units, range: NSMakeRange(offset, FORMAT_UINT16))
                offset += 2
                rrIntervals.append(Float(units) * 1024.0 / 1000.0)
            }
        }
        print("间期:" + "\(rrIntervals)")
    }
}
